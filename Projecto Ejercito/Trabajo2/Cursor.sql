/*==========================================================================================*/
/*  Cursor Qué muestre por cada tipo de salida de militares, cuantas en total han ocurrido. */
/*==========================================================================================*/

select misiones_ejercito.tipo_mision, count(id_mision) from misiones_ejercito group by 1;

do $$
declare
	num int;

	cursor1 cursor is 
	select misiones_ejercito.tipo_mision, count(id_mision) as contar from misiones_ejercito group by 1;
begin 
	for num in cursor1 loop
		raise notice 'Tipo de mision: %, Numero_misiones: %',
			num.tipo_mision, num.contar;
	end loop;
end $$
language 'plpgsql';

