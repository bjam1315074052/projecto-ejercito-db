/*===========================================================================================*/
/* Trigger. Que prevenga insertar un retiro, cuando ese soldado deba estar en alguna misión. */ 
/*===========================================================================================*/

CREATE OR REPLACE FUNCTION TRIGGER1() RETURNS TRIGGER
AS
$BODY$
DECLARE
	CONTAR INT;
BEGIN
	select count(line_misiones.id_integrante) INTO CONTAR from integrante_ejercito
	inner join line_misiones on line_misiones.id_integrante = integrante_ejercito.id_integrante
	WHERE integrante_ejercito.id_integrante = new.id_integrante;
	
	IF(CONTAR >= 1)THEN
		RAISE EXCEPTION 'No puede realizar el ingreso';
	end if;
	return new;
end;
$BODY$
language plpgsql;

create trigger TRIGGER1 before 
insert on retiro_ejercito for each row
execute procedure TRIGGER1();

INSERT INTO retiro_ejercito(
	id_retiro, id_integrante, tipo_de_retiro, fecha_retiro, investigacion_retiro)
	VALUES 
	(14, 28, 'Jubilacion', '07/11/2020', 'Junta Formada');
	
--delete from retiro_ejercito where id_retiro = 13;
